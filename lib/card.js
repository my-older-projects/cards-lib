'use strict';

module.exports = class Card {
	constructor(opts) {
		opts = opts || {};

		this.suit = opts.suit; // can be names // 'spade', 'leaf', 'club'
		this.value = opts.value; // can be names // 'jack', 10, '10', 'J'

		this._fillDetails();
	}

	_fillDetails() {
		if (this.suit && this.value) {
			this.suit = (this.suit.length > 1) ? Card.nameToCode(this.suit) : this.unit;
			this.value = ((typeof this.value === 'number') && this.value.toString()) || (((this.value.length > 1) && Card.nameToCode(this.value)) || this.value.toUpperCase());
			this.id = this.value.concat(this.suit);
		} else {
			throw new Error('Insufficent Data to create the card.');
		}

		this.name = (({
			2: 'Two',
			3: 'Three',
			4: 'Four',
			5: 'Five',
			6: 'Six',
			7: 'Seven',
			8: 'Eight',
			9: 'Nine',
			10: 'Ten',
			A: 'Ace',
			J: 'Jack',
			Q: 'Queen',
			K: 'King'
		})[this.value]).concat(' of ').concat(Card.codeToName(this.suit).split('').map((val, index) => (index === 0 ? val.toUpperCase() : val)).join('')).concat('s');
		this.unicodeCard = Card.getSpecificUnicode(this.value, this.suit);
		this.back = Card.cardBack();

		this.addToDeck();
	}

	addToDeck() {
		this.state = 'decked';
	}

	draw() {
		this.state = 'drawn';
	}

	discard() {
		this.state = 'discarded';
	}

	static cardBack() {
		return '🂠';
	}

	static nameToCode(str) {
		const lk = {
			ace: 'A',
			king: 'K',
			queen: 'Q',
			jack: 'J'
		};

		const mk = {
			club: Card.unicode().leaves,
			diamond: Card.unicode().dias,
			heart: Card.unicode().hearts,
			spade: Card.unicode().spades,
			leaf: Card.unicode().leaves
		};

		return lk[str] || mk[str];
	}

	static codeToName(str) {
		const mk = {
			'♠': 'spade',
			'♥': 'heart',
			'♦': 'diamond',
			'♣': 'club'
		};

		return mk[str];
	}

	static unicode() {
		return {
			hearts: '♥',
			spades: '♠',
			dias: '♦',
			leaves: '♣'
		};
	}

	static getSpecificUnicode(value, suit) {
		const suits = {
			'♠': '1F0A',
			'♥': '1F0B',
			'♦': '1F0C',
			'♣': '1F0D'
		};

		const values = {
			A: '1',
			2: '2',
			3: '3',
			4: '4',
			5: '5',
			6: '6',
			7: '7',
			8: '8',
			9: '9',
			10: 'A',
			J: 'B',
			C: 'C',
			Q: 'D',
			K: 'E'
		};

		return String.fromCodePoint(parseInt(suits[suit].concat(values[value]), 16));
	}
};
