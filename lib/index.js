'use strict';
const Card = require('./card');
const Deck = require('./deck');

let options = {};

exports.createDeck = build => new Deck(build || options.deckType);
exports.createCard = opts => new Card(opts);

exports.setDefaultDeck = build => options.deckType = build;
