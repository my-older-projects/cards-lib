'use strict';
const Card = require('./card');
const xorshift = require('xorshift');

/**
 * http://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array-in-javascript
 * Changed Math.random() with xorshift
 * Shuffles array in place.
 * @param {Array} array items The array containing the items.
 * @return {Array} array The shuffled array
 */
function shuffle(array) {
	let counter = array.length;

	// While there are elements in the array
	while (counter > 0) {
		// Pick a random index
		let index = Math.floor(xorshift.random() * counter);

		// Decrease counter by 1
		counter--;

		// And swap the last element with it
		let temp = array[counter];
		array[counter] = array[index];
		array[index] = temp;
	}

	return array;
}

class Deck {
	constructor(build) {
		this.deck = [];
		this.discarded = [];
		this.drawn = [];
		this.total = [];

		(typeof build === 'string') && (build = this._workers()[build]);
		build && build();
	}

	add(card) {
		card.addToDeck();
		this.total.push(card);
		this.deck.push(card);
	}

	discard() {
		let card = this.draw();
		card.discard();
		this.discarded.push(card);
		return card;
	}

	draw() {
		if (!this.deck.length) {
			throw new RangeError('No card to draw');
		}
		let card = this.deck.shift();
		card.draw();
		this.drawn.push(card);
		return card;
	}

	remaining() {
		return this.deck.length;
	}

	shuffleAll() {
		this.deck = this.deck.concat(this.drawn.forEach(card => card.addToDeck())).concat(this.discarded.forEach(card => card.addToDeck()));
		this.drawn = this.discarded = [];
		this.shuffleRest();
	}

	shuffleDiscarded() {
		this.discarded = shuffle(this.discarded);
	}

	shuffleRest() {
		this.deck = shuffle(this.deck);
	}

	_workers() {
		let that = this;
		return {
			poker: () => {
				['club', 'diamond', 'heart', 'spade'].forEach(suit => {
					[2, 3, 4, 5, 6, 7, 8, 9, 10, 'J', 'Q', 'K', 'A'].forEach(value => {
						that.add(new Card({suit: suit, value: value}));
					});
				});
			}
		};
	}
}

module.exports = Deck;
